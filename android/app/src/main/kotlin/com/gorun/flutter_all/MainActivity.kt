package com.gorun.flutter_all

import androidx.annotation.NonNull
import com.example.plugin.asr.AsrPlugin
import io.flutter.embedding.android.FlutterActivity
import io.flutter.embedding.engine.FlutterEngine
import io.flutter.embedding.engine.plugins.shim.ShimPluginRegistry
import io.flutter.plugins.GeneratedPluginRegistrant

class MainActivity: FlutterActivity() {

    override fun configureFlutterEngine(@NonNull flutterEngine: FlutterEngine) {
        val shimPluginRegistry = ShimPluginRegistry(flutterEngine)
        GeneratedPluginRegistrant.registerWith(flutterEngine);
        AsrPlugin.registerWith(shimPluginRegistry.registrarFor("com.example.plugin.asr.AsrPlugin"))
    }

}
