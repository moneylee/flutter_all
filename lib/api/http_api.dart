class HttpApi {
  static final String BaesUrl_normal = "http://mock.moneylee.cn/mock/5f732abb191a2b0020312c7e/study";
  static final String BaseUrl_01 = "https://m.ctrip.com/restapi/h5api";

  //轮播图
  static final String swiper = BaesUrl_normal + "/swiper";

  //搜索
  static final String search = BaseUrl_01 + "/globalsearch/search";

  //旅拍
  static final String travle_tabbar = BaesUrl_normal + "/travle_tabbar";
  //https://www.devio.org/io/flutter_app/json/travel_page.json

}