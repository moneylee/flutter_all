import 'package:connectivity/connectivity.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_all/api/http_config.dart';

import 'http_api.dart';

typedef HttpSuccessCallback<T> = void Function(dynamic data);
//请求失败的回调
typedef HttpFailureCallback = void Function(HttpError data);

class HttpManager {

  //创建一个单例
  static final HttpManager _instance = HttpManager._internal();
  factory HttpManager() => _instance;
  Dio _dio;
  static const CODE_SUCCESS = 200;
  static const CODE_TIME_OUT = -1;

  HttpManager._internal({String baseUrl}) {
    if (_dio == null) {
      // 全局属性：请求前缀、连接超时时间、响应超时时间
      BaseOptions options = BaseOptions(
        baseUrl: HttpApi.BaesUrl_normal,
        connectTimeout: HttpConfig.CONNECT_TIMEOUT,
        receiveTimeout: HttpConfig.RECEIVE_TIMEOUT,
      );
      _dio = Dio(options);
    }
  }

  static HttpManager getInstance({String baseUrl}) {
    if (baseUrl == null) {
      return _instance._normal();
    } else {
      return _instance._baseUrl(baseUrl);
    }
  }

  //用于指定特定域名
  HttpManager _baseUrl(String baseUrl) {
    if (_dio != null) {
      _dio.options.baseUrl = HttpApi.BaseUrl_01;
    }
    return this;
  }

  //一般请求，默认域名
  HttpManager _normal() {
    if (_dio != null) {
      if (_dio.options.baseUrl != HttpApi.BaesUrl_normal) {
        _dio.options.baseUrl = HttpApi.BaesUrl_normal;
      }
    }
    return this;
  }

  /**
   * 初始化公共属性
   * [baseUrl] 地址前缀
   * [connectTimeout] 连接超时赶时间
   * [receiveTimeout] 接收超时赶时间
   * [interceptors] 基础拦截器
   */
  // void init(
  //     {String baseUrl,
  //       int connectTimeout,
  //       int receiveTimeout,
  //       List<Interceptor> interceptors}) {
  //   _dio.options = _dio.options.merge(
  //     baseUrl: baseUrl,
  //     connectTimeout: connectTimeout,
  //     receiveTimeout: receiveTimeout,
  //   );
  //   if (interceptors != null && interceptors.isNotEmpty) {
  //     _dio.interceptors..addAll(interceptors);
  //   }
  // }

  /**
   * GET异步网络请求
   * [url] 网络请求地址不包含域名
   * [params] url请求参数支持restful
   * [options] 请求配置
   * [tag] 请求统一标识，用于取消网络请求
   */
  Future<Map<String, dynamic>> getAsync<T>({
    @required String url,
    Map<String, dynamic> params,
    Options options,
    @required String tag,
  }) async {
    return _requestAsync(
      url: url,
      method: "GET",
      params: params,
      options: options,
      tag: tag,
    );
  }

  ///POST 异步网络请求
  ///
  ///[url] 网络请求地址不包含域名
  ///[data] post 请求参数
  ///[params] url请求参数支持restful
  ///[options] 请求配置
  ///[tag] 请求统一标识，用于取消网络请求
  Future<Map<String, dynamic>> postAsync<T>({
    @required String url,
    data,
    Map<String, dynamic> params,
    Options options,
    @required String tag,
  }) async {
    return _requestAsync(
      url: url,
      method: "POST",
      data: data,
      params: params,
      options: options,
      tag: tag,
    );
  }


  /**
   * 统一网络请求
   * [url] 网络请求地址不包含域名
   * [data] post 请求参数
   * [params] url请求参数支持restful
   * [options] 请求配置
   * [tag] 请求统一标识，用于取消网络请求
   */
  Future<Map<String, dynamic>> _requestAsync<T>({
    @required String url,
    String method,
    data,
    Map<String, dynamic> params,
    Options options,
    @required String tag,
  }) async {
    //检查网络是否连接
    ConnectivityResult connectivityResult =
    await (Connectivity().checkConnectivity());
    if (connectivityResult == ConnectivityResult.none) {
      throw (HttpError(HttpError.NETWORK_ERROR, "网络异常，请稍后重试！"));
    }

    //设置默认值
    params = params ?? {};
    method = method ?? 'GET';

    options?.method = method;

    options = options ??
    Options(
      method: method,
    );

    url = _restfulUrl(url, params);

    try {
      Response<Map<String, dynamic>> response = await _dio.request(url,
          queryParameters: params,
          data: data,
          options: options);
      int statusCode = response.data["status"];
      int statusI = response.statusCode;
      if (statusCode == 0 || statusI == 200) {
        //成功
        return response.data;
      } else {
        //失败
        String message = response.data["msg"];
        return Future.error((HttpError(statusCode.toString(), message)));
      }
    } on DioError catch (e, s) {
      throw (HttpError.dioError(e));
    } catch (e, s) {
      // LogUtil.v("未知异常出错：$e\n$s");
      throw (HttpError(HttpError.UNKNOWN, "网络异常，请稍后重试！"));
    }
  }

  ///restful处理
  String _restfulUrl(String url, Map<String, dynamic> params) {
    // restful 请求处理
    // /gysw/search/hist/:user_id        user_id=27
    // 最终生成 url 为     /gysw/search/hist/27
    params.forEach((key, value) {
      if (url.indexOf(key) != -1) {
        url = url.replaceAll(':$key', value.toString());
      }
    });
    return url;
  }

/**
 * 中文乱码
 */
  // void xx() {
  //   Utf8Decoder utf8decoder = Utf8Decoder(); //fix 中文乱码
  //   var result = json.decode(utf8decoder.convert(response.bodyBytes));
  //   return result
  // }

}


