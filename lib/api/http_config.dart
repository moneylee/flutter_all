import 'package:dio/dio.dart';




/**
 * http配置相关
 */
class HttpConfig {

  //超时相关
  static const int CONNECT_TIMEOUT = 30000;
  static const int RECEIVE_TIMEOUT = 30000;

}



/**
 * http错误相关
 */
class HttpError {

  String code;
  String message;

  HttpError(this.code, this.message);

  @override
  String toString() {
    return 'HttpError{code: $code, messgae: $message}';
  }

  /**
   * 自定义的网络故障
   */
  ///未知错误
  static const String UNKNOWN = "UNKNOWN";
  ///解析错误
  static const String PARSE_ERROR = "PARSE_ERROR";
  ///网络错误
  static const String NETWORK_ERROR = "NETWORK_ERROR";
  ///协议错误
  static const String HTTP_ERROR = "HTTP_ERROR";
  ///证书错误
  static const String SSL_ERROR = "SSL_ERROR";
  ///连接超时
  static const String CONNECT_TIMEOUT = "CONNECT_TIMEOUT";
  ///响应超时
  static const String RECEIVE_TIMEOUT = "RECEIVE_TIMEOUT";
  ///发送超时
  static const String SEND_TIMEOUT = "SEND_TIMEOUT";
  ///网络请求取消
  static const String CANCEL = "CANCEL";


  /**
   * dio的网络故障
   */
  HttpError.dioError(DioError error) {
    message = error.message;
    switch (error.type) {
      case DioErrorType.CONNECT_TIMEOUT:
        code = CONNECT_TIMEOUT;
        message = "网络连接超时，请检查网络设置";
        break;
      case DioErrorType.RECEIVE_TIMEOUT:
        code = RECEIVE_TIMEOUT;
        message = "服务器异常，请稍后重试！";
        break;
      case DioErrorType.SEND_TIMEOUT:
        code = SEND_TIMEOUT;
        message = "网络连接超时，请检查网络设置";
        break;
      case DioErrorType.RESPONSE:
        code = HTTP_ERROR;
        message = "服务器异常，请稍后重试！";
        break;
      case DioErrorType.CANCEL:
        code = CANCEL;
        message = "请求已被取消，请重新请求";
        break;
      case DioErrorType.DEFAULT:
        code = UNKNOWN;
        message = "网络异常，请稍后重试！";
        break;
    }
  }
}