import 'package:flutter_all/generated/json/base/json_convert_content.dart';

class HomeEntity with JsonConvert<HomeEntity> {
	HomeConfig config;
	List<HomeBannerList> bannerList;
	List<HomeLocalNavList> localNavList;
	HomeGridNav gridNav;
	List<HomeSubNavList> subNavList;
	HomeSalesBox salesBox;
}

class HomeConfig with JsonConvert<HomeConfig> {
	String searchUrl;
}

class HomeBannerList with JsonConvert<HomeBannerList> {
	String icon;
	String url;
}

class HomeLocalNavList with JsonConvert<HomeLocalNavList> {
	String icon;
	String title;
	String url;
	String statusBarColor;
	bool hideAppBar;
}

class HomeGridNav with JsonConvert<HomeGridNav> {
	HomeGridNavHotel hotel;
	HomeGridNavFlight flight;
	HomeGridNavTravel travel;
}

class HomeGridNavHotel with JsonConvert<HomeGridNavHotel> {
	String startColor;
	String endColor;
	HomeGridNavHotelMainItem mainItem;
	HomeGridNavHotelItem1 item1;
	HomeGridNavHotelItem2 item2;
	HomeGridNavHotelItem3 item3;
	HomeGridNavHotelItem4 item4;
}

class HomeGridNavHotelMainItem with JsonConvert<HomeGridNavHotelMainItem> {
	String title;
	String icon;
	String url;
	String statusBarColor;
}

class HomeGridNavHotelItem1 with JsonConvert<HomeGridNavHotelItem1> {
	String title;
	String url;
	String statusBarColor;
}

class HomeGridNavHotelItem2 with JsonConvert<HomeGridNavHotelItem2> {
	String title;
	String url;
}

class HomeGridNavHotelItem3 with JsonConvert<HomeGridNavHotelItem3> {
	String title;
	String url;
	bool hideAppBar;
}

class HomeGridNavHotelItem4 with JsonConvert<HomeGridNavHotelItem4> {
	String title;
	String url;
	bool hideAppBar;
}

class HomeGridNavFlight with JsonConvert<HomeGridNavFlight> {
	String startColor;
	String endColor;
	HomeGridNavFlightMainItem mainItem;
	HomeGridNavFlightItem1 item1;
	HomeGridNavFlightItem2 item2;
	HomeGridNavFlightItem3 item3;
	HomeGridNavFlightItem4 item4;
}

class HomeGridNavFlightMainItem with JsonConvert<HomeGridNavFlightMainItem> {
	String title;
	String icon;
	String url;
}

class HomeGridNavFlightItem1 with JsonConvert<HomeGridNavFlightItem1> {
	String title;
	String url;
	bool hideAppBar;
}

class HomeGridNavFlightItem2 with JsonConvert<HomeGridNavFlightItem2> {
	String title;
	String url;
}

class HomeGridNavFlightItem3 with JsonConvert<HomeGridNavFlightItem3> {
	String title;
	String url;
	bool hideAppBar;
}

class HomeGridNavFlightItem4 with JsonConvert<HomeGridNavFlightItem4> {
	String title;
	String url;
	bool hideAppBar;
}

class HomeGridNavTravel with JsonConvert<HomeGridNavTravel> {
	String startColor;
	String endColor;
	HomeGridNavTravelMainItem mainItem;
	HomeGridNavTravelItem1 item1;
	HomeGridNavTravelItem2 item2;
	HomeGridNavTravelItem3 item3;
	HomeGridNavTravelItem4 item4;
}

class HomeGridNavTravelMainItem with JsonConvert<HomeGridNavTravelMainItem> {
	String title;
	String icon;
	String url;
	bool hideAppBar;
	String statusBarColor;
}

class HomeGridNavTravelItem1 with JsonConvert<HomeGridNavTravelItem1> {
	String title;
	String url;
	String statusBarColor;
	bool hideAppBar;
}

class HomeGridNavTravelItem2 with JsonConvert<HomeGridNavTravelItem2> {
	String title;
	String url;
	String statusBarColor;
	bool hideAppBar;
}

class HomeGridNavTravelItem3 with JsonConvert<HomeGridNavTravelItem3> {
	String title;
	String url;
	bool hideAppBar;
}

class HomeGridNavTravelItem4 with JsonConvert<HomeGridNavTravelItem4> {
	String title;
	String url;
	bool hideAppBar;
}

class HomeSubNavList with JsonConvert<HomeSubNavList> {
	String icon;
	String title;
	String url;
	bool hideAppBar;
}

class HomeSalesBox with JsonConvert<HomeSalesBox> {
	String icon;
	String moreUrl;
	HomeSalesBoxBigCard1 bigCard1;
	HomeSalesBoxBigCard2 bigCard2;
	HomeSalesBoxSmallCard1 smallCard1;
	HomeSalesBoxSmallCard2 smallCard2;
	HomeSalesBoxSmallCard3 smallCard3;
	HomeSalesBoxSmallCard4 smallCard4;
}

class HomeSalesBoxBigCard1 with JsonConvert<HomeSalesBoxBigCard1> {
	String icon;
	String url;
	String title;
}

class HomeSalesBoxBigCard2 with JsonConvert<HomeSalesBoxBigCard2> {
	String icon;
	String url;
	String title;
}

class HomeSalesBoxSmallCard1 with JsonConvert<HomeSalesBoxSmallCard1> {
	String icon;
	String url;
	String title;
}

class HomeSalesBoxSmallCard2 with JsonConvert<HomeSalesBoxSmallCard2> {
	String icon;
	String url;
	String title;
}

class HomeSalesBoxSmallCard3 with JsonConvert<HomeSalesBoxSmallCard3> {
	String icon;
	String url;
	String title;
}

class HomeSalesBoxSmallCard4 with JsonConvert<HomeSalesBoxSmallCard4> {
	String icon;
	String url;
	String title;
}
