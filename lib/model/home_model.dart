
import 'package:flutter_all/model/config_model.dart';
import 'package:flutter_all/model/grid_nav_model.dart';
import 'package:flutter_all/model/sales_box_model.dart';

import 'common_model.dart';

class HomeModel {

  final ConfigModel configModel;
  final List<CommonModel> bannerList;
  final List<CommonModel> subNavList;
  final List<CommonModel> localNavList;
  final GridNavModel gridNav;
  final SalesBoxModel salesBox;

  HomeModel({this.configModel,this.localNavList, this.bannerList, this.subNavList, this.gridNav, this.salesBox});

  factory HomeModel.fromJson(Map<String, dynamic> json) {

    var localNavListJson = json["localNavList"] as List;
    List<CommonModel> localNavList = localNavListJson.map((e) {
      return CommonModel.fromJson(e);
    }).toList();

    var bannerListJson = json["bannerList"] as List;
    List<CommonModel> bannerList = bannerListJson.map((e) {
      return CommonModel.fromJson(e);
    }).toList();

    var subNavListJson = json["subNavList"] as List;
    List<CommonModel> subNavList = subNavListJson.map((e) {
      return CommonModel.fromJson(e);
    }).toList();

    return HomeModel(
      configModel: ConfigModel.fromJson(json["config"]),
      gridNav: GridNavModel.fromJson(json["gridNav"]),
      salesBox: SalesBoxModel.fromJson(json["salesBox"]),
      localNavList: localNavList,
      subNavList: subNavList,
      bannerList: bannerList
    );
  }

}