import 'package:flutter_all/generated/json/base/json_convert_content.dart';

class TravelEntity with JsonConvert<TravelEntity> {
	String url;
	TravelParams params;
	List<TravelTab> tabs;
}

class TravelParams with JsonConvert<TravelParams> {
	int districtId;
	String groupChannelCode;
	dynamic type;
	int lat;
	int lon;
	int locatedDistrictId;
	TravelParamsPagePara pagePara;
	int imageCutType;
	TravelParamsHead head;
	String contentType;
}

class TravelParamsPagePara with JsonConvert<TravelParamsPagePara> {
	int pageIndex;
	int pageSize;
	int sortType;
	int sortDirection;
}

class TravelParamsHead with JsonConvert<TravelParamsHead> {
	String cid;
	String ctok;
	String cver;
	String lang;
	String sid;
	String syscode;
	dynamic auth;
	List<TravelParamsHeadExtension> extension;
}

class TravelParamsHeadExtension with JsonConvert<TravelParamsHeadExtension> {
	String name;
	String value;
}

class TravelTab with JsonConvert<TravelTab> {
	String labelName;
	String groupChannelCode;
}
