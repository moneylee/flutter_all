import 'package:flutter_all/generated/json/base/json_convert_content.dart';

class SearchEntity with JsonConvert<SearchEntity> {
	List<SearchData> data;
}

class SearchData with JsonConvert<SearchData> {
	String code;
	String word;
	String type;
	String districtname;
	String url;
	String price;
	String zonename;
	String star;
}
