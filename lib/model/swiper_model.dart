class SwiperModel {
  final String url;
  final String name;
  final String img;
  SwiperModel({this.url, this.name, this.img});
  factory SwiperModel.fromJson(Map<String, dynamic> json) {
    return SwiperModel(
      url: json["icon"],
      name: json["name"],
      img: json["img"]
    );
  }
}