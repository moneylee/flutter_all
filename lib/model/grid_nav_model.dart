import 'package:flutter_all/model/common_model.dart';

class GridNavModel {
  final GriNavItem hotel;
  final GriNavItem flight;
  final GriNavItem travel;
  GridNavModel({this.hotel, this.flight, this.travel});
  factory GridNavModel.fromJson(Map<String, dynamic> json) {
    return json != null ?
      GridNavModel(
        hotel: GriNavItem.fromJson(json['hotel']),
        flight: GriNavItem.fromJson(json['flight']),
        travel: GriNavItem.fromJson(json['travel']),
      )
    :null;

  }
}


class GriNavItem {
  final String startColor;
  final String endColor;
  final CommonModel mainItem;
  final CommonModel item1;
  final CommonModel item2;
  final CommonModel item3;
  final CommonModel item4;
  GriNavItem({this.startColor, this.endColor, this.mainItem, this.item1,
    this.item2, this.item3, this.item4});
  factory GriNavItem.fromJson(Map<String, dynamic> json) {
    return GriNavItem(
      startColor: json["startColor"],
      endColor: json[""],
      mainItem: CommonModel.fromJson(json["mainItem"]),
      item1: CommonModel.fromJson(json["item1"]),
      item2: CommonModel.fromJson(json["item2"]),
      item3: CommonModel.fromJson(json["item3"]),
      item4: CommonModel.fromJson(json["item4"]),

    );
  }
}