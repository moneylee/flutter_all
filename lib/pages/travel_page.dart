import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_all/api/http_api.dart';
import 'package:flutter_all/api/http_manager.dart';
import 'package:flutter_all/model/home_entity.dart';
import 'package:flutter_all/model/home_model.dart';
import 'package:flutter_all/model/travel_entity.dart';
import 'package:flutter_all/pages/travel_tab_page.dart';


class TravelPage extends StatefulWidget {
  @override
  _TravelPageState createState() => _TravelPageState();
}

class _TravelPageState extends State<TravelPage> with TickerProviderStateMixin {
  TabController _controller;
  List<TravelTab> travelTabList = [];
  TravelEntity travelModel;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("首页"),
      ),
      body: Column(
        children: [
          Container(
            color: Colors.white,
            padding: EdgeInsets.only(top: 0),
            child: TabBar(
              controller: _controller,
              isScrollable: true,
              labelColor: Colors.black,
              labelPadding: EdgeInsets.fromLTRB(20, 0, 10, 5),
              indicator: UnderlineTabIndicator(
                borderSide: BorderSide(color: Colors.red, width: 3),
                insets: EdgeInsets.fromLTRB(0, 0, 0, 10)
              ),
              tabs: travelTabList.map<Tab>((TravelTab tab) {
                return Tab(text: tab.labelName,);
              }).toList(),
            ),
          ),
          Flexible(
            child: TabBarView(
              controller: _controller,
              children: travelTabList.map((TravelTab tab) {
                return TravelTabPage(travelUrl: null, groupChannelCode: tab.groupChannelCode);
              }).toList(),
            ),
          )
        ],
      )
    );
  }

  @override
  void initState() {
    _controller = TabController(length: travelTabList.length, vsync: this);
    _loadData();
    super.initState();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  void _loadData() {
    HttpManager().getAsync(url: HttpApi.travle_tabbar, tag: "travel_tabbar").then((value) {
      TravelEntity travelEntity = TravelEntity().fromJson(value);
      print(travelEntity.toJson());
      _controller = TabController(length: travelEntity.tabs.length, vsync: this);
      setState(() {
        travelTabList = travelEntity.tabs;
        travelModel = travelEntity;
      });
    }).catchError((error) {
      print(error);
    });
  }

}


