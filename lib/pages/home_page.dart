import 'package:flutter/material.dart';
import 'package:flutter_all/api/http_api.dart';
import 'package:flutter_all/api/http_manager.dart';
import 'package:flutter_all/model/grid_nav_model.dart';
import 'package:flutter_all/model/home_entity.dart';
import 'package:flutter_all/pages/search_page.dart';
import 'package:flutter_all/pages/speak_page.dart';
import 'package:flutter_all/wigets/grid_card.dart';
import 'package:flutter_all/wigets/grid_nav.dart';
import 'package:flutter_all/wigets/search_bar.dart';
import 'package:flutter_all/wigets/sub_nav.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';


class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> with AutomaticKeepAliveClientMixin {
  List swiperList = [];
  List<HomeLocalNavList> localNavList = [];
  List<HomeSubNavList> subNavList = [];
  HomeGridNav gridNavModel;
  double appBarAlpha = 0;
  @override
  bool get wantKeepAlive => true;
  @override
  void initState() {
    super.initState();
    _loadData();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        // appBar: _appBar,
        body: Container(
          child: Stack(
            children: [
              MediaQuery.removePadding(
                  removeTop: true,
                  context: context,
                  child: NotificationListener(
                    onNotification: (scrollNotication) {
                      if (scrollNotication is ScrollUpdateNotification && scrollNotication.depth == 0) {
                        _onScroll(scrollNotication.metrics.pixels);
                      }
                    },
                    child: ListView(
                      children: [
                        Container(
                          height: 380.w,
                          child: Swiper(
                            key: UniqueKey(),
                            pagination: SwiperPagination(),
                            onTap: (index){},
                            itemCount: swiperList.length,
                            autoplay: true,
                            itemBuilder: (ctx, index){
                              return Image.network(swiperList[index].icon, fit: BoxFit.fill,);
                            },
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.fromLTRB(7, 4, 7, 4),
                          child: GridNavWidget(localNavList: localNavList,),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 7, right: 7),
                          child: GridCard(gridNavModel),
                        ),
                        SizedBox(height: 5),
                        SubNavWidget(subNavList: subNavList,),
                        Container(
                          height: 300,
                          color: Colors.yellow,
                        )


                      ],
                    ),
                  )
              ),
              _appBar
            ],
          ),
        )
    );
  }

  void _loadData() {
    HttpManager.getInstance().getAsync(url: "/home", tag: "home").then((value) {
      HomeEntity homeEntity = HomeEntity().fromJson(value);
      setState(() {
        localNavList = homeEntity.localNavList;
        swiperList = homeEntity.bannerList;
        gridNavModel = homeEntity.gridNav;
        subNavList = homeEntity.subNavList;
      });
    }).catchError((error) {
      print(error);
    });
  }

  //监听滚动
  _onScroll(offset){
    double alpha = offset/100;
    if(alpha<0){
      alpha = 0;
    }else if(alpha>1) {
      alpha = 1;
    }
    setState(() {
      appBarAlpha = alpha;
    });
  }

  Widget get _appBar {
    return Column(
      children: [
        Container(
          decoration: BoxDecoration(
            gradient: LinearGradient(
              colors: [Color(0x66000000), Colors.transparent],
              begin: Alignment.topCenter,
              end: Alignment.bottomCenter
            )
          ),
          child: Container(
            padding: EdgeInsets.fromLTRB(0, 20, 0, 0),
            height: 80.0,
            decoration: BoxDecoration(
              color: Color.fromARGB((appBarAlpha * 255).toInt(), 255, 255, 255)
            ),
            child: SearchBar(
              searchBarType: appBarAlpha > 0.2 ? SearchBarType.homeLight : SearchBarType.home,
              inputBoxClick: _jumpToSeach,
              speakClick: _jumpToSpeak,
              defaultText: "欢迎光临！",
              leftButtonClick: () {},
            ),
          ),
        ),
        Container(
          height: appBarAlpha > 0.2 ? 0.5 : 0,
          decoration: BoxDecoration(
            boxShadow: [BoxShadow(color: Colors.black12, blurRadius: 0.5)]
          ),
        )
      ],
    );
  }

  _jumpToSeach() {
    Navigator.push(context, MaterialPageRoute(builder: (context){
      return SearchPage();
    }));
  }

  _jumpToSpeak() {

    Navigator.push(context, MaterialPageRoute(builder: (context){
      return SpeakPage();
    }));
  }



}



