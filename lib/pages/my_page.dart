import 'package:flutter/material.dart';
import 'package:flutter_all/wigets/web_view.dart';

class MyPage extends StatefulWidget {
  @override
  _MyPageState createState() => _MyPageState();
}

class _MyPageState extends State<MyPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: MediaQuery.removePadding(
        removeTop: true,
        context: context,
        child: WebView(
          url: "https://m.ctrip.com/webapp/myctrip/",
          hedeAppBar: true,
          backForbid: false,
        ),
      )
    );
  }
}


