import 'dart:convert';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter_all/api/http_manager.dart';
import 'package:flutter_all/model/travel_page_entity.dart';
import 'package:flutter_all/utils/navigator_util.dart';
import 'package:flutter_all/wigets/loading_container.dart';
import 'package:flutter_all/wigets/web_view.dart';
import 'package:flutter_easyrefresh/easy_refresh.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';


const TRAVEL_URL = "https://m.ctrip.com/restapi/soa2/16189/json/searchTripShootListForHomePageV2?_fxpcqlniredt=09031014111431397988&__gw_appid=99999999&__gw_ver=1.0&__gw_from=10650013707&__gw_platform=H5";
const PAGE_SIZE = 10;
var Params = {
  "districtId": -1,
  "groupChannelCode": "RX-OMF",
  "type": null,
  "lat": -180,
  "lon": -180,
  "locatedDistrictId": 0,
  "pagePara": {
    "pageIndex": 1,
    "pageSize": 10,
    "sortType": 9,
    "sortDirection": 0
  },
  "imageCutType": 1,
  "head": {'cid': "09031014111431397988"},
  "contentType": "json"
};


class TravelTabPage extends StatefulWidget {
  final String travelUrl;
  final String groupChannelCode;
  TravelTabPage({this.travelUrl, this.groupChannelCode});
  @override
  _TravelTabPageState createState() => _TravelTabPageState();

}

class _TravelTabPageState extends State<TravelTabPage> with AutomaticKeepAliveClientMixin{
  int pageIndex = 1;
  bool isLoading = true;
  List<TravelPageResultListArticle> travelItems = [];
  EasyRefreshController _controller = EasyRefreshController();
  @override
  void initState() {
    _loadData();
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: LoadingContainer(
        isLoading: isLoading,
        child: EasyRefresh(
          controller: _controller,
          onRefresh: () async {
            setState(() {
              pageIndex = 1;
            });
            _loadData();
          },
          onLoad: () async {
            setState(() {
              pageIndex += 1;
            });
            _loadData();
          },
          child: StaggeredGridView.countBuilder(
            crossAxisCount: 4,
            itemCount: travelItems.length,
            itemBuilder: (BuildContext context, int index) {
              return GestureDetector(
                onTap: () {
                  // Navigator.push(context,
                  //     MaterialPageRoute(builder: (ctx) {
                  //       return WebView(url: homeLocalNavList.url, hedeAppBar: homeLocalNavList.hideAppBar, title: homeLocalNavList.title,);
                  //     })
                  // );
                  NavigatorUtil.push(
                      context,WebView(
                    url: travelItems[index].urls[0].h5Url,
                    title: '详情',
                  ));
                },
                child: Card(
                  child: PhysicalModel(
                    color: Colors.transparent,
                    clipBehavior: Clip.antiAlias,
                    borderRadius: BorderRadius.circular(5),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        _itemImage(travelItems[index]),
                        Container(
                          padding: EdgeInsets.all(4),
                          child: Text(
                            travelItems[index].articleTitle,
                            maxLines: 1,
                            overflow: TextOverflow.ellipsis,
                            style: TextStyle(fontSize: 14, color: Colors.black87),
                          ),
                        ),
                        _infoText(travelItems[index])
                      ],
                    ),
                  ),

                ),
              );
            },
            staggeredTileBuilder: (int index) => new StaggeredTile.fit(2),
            // mainAxisSpacing: 4.0,
            // crossAxisSpacing: 4.0,
          ),
        ),
      )
    );
  }

  _loadData() {
    Map paramsMap = Params['pagePara'];
    paramsMap['pageIndex'] = pageIndex;
    paramsMap['pageSize'] = PAGE_SIZE;
    Params['groupChannelCode'] = widget.groupChannelCode;
    HttpManager().postAsync(url: TRAVEL_URL, tag: "travel_model", data: Params).then((value) {
      TravelPageEntity travelPageEntity = TravelPageEntity().fromJson(value);
        setState(() {
          isLoading = false;
          List<TravelPageResultListArticle> items = _filterItems(travelPageEntity.resultList);
          if (pageIndex == 1) {
            travelItems.clear();
            _controller.finishRefresh(success: true);
            _controller.finishLoad(success: true, noMore: false);
          } else {
            if (pageIndex == 3) {
              _controller.finishLoad(success: true, noMore: true);
            } else {
              _controller.finishLoad(success: true, noMore: false);
            }
          }
          travelItems.addAll(items);
        });
    }).catchError((e) {
      setState(() {
        isLoading = false;
      });
    });
  }

  List<TravelPageResultListArticle> _filterItems(List<TravelPageResultList> resultList) {
    if(resultList == null) return [];
    List<TravelPageResultListArticle> filterItems = [];
    resultList.forEach((element) {
      if (element.article!=null) {
        filterItems.add(element.article);
      }
    });
    return filterItems;
  }

  _itemImage(TravelPageResultListArticle item) {
    return Stack(
      children: [
        Image.network(item.images[0].dynamicUrl),
        Positioned(
          bottom: 8,
          left: 8,
          child: Container(
            padding: EdgeInsets.fromLTRB(5, 1, 5, 1),
            decoration: BoxDecoration(
              color: Colors.black54,
              borderRadius: BorderRadius.circular(10),
            ),
            child: Row(
              children: [
                Padding(
                  padding: EdgeInsets.only(right: 3),
                  child: Icon(Icons.location_on, color: Colors.white, size: 12,),
                ),
                LimitedBox(
                  maxWidth: 130,
                  child: Text(
                    _poiName(item.pois),
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(color: Colors.white, fontSize: 12),
                  ),
                )

              ],
            ),
          ),
        )
      ],
    );
  }

  String _poiName(List<TravelPageResultListArticlePoi> items) {
    return (items == null || items.length == 0)?"位置": items[0].poiName;
  }

  _infoText(TravelPageResultListArticle item) {
    return Container(
      padding: EdgeInsets.fromLTRB(6,0,6,10),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Row(
            children: [
              PhysicalModel(
                color: Colors.transparent,
                clipBehavior: Clip.antiAlias,
                borderRadius: BorderRadius.circular(12),
                child: Image.network(item.author.coverImage.dynamicUrl, width: 24, height: 24,),
              ),
              Container(
                padding: EdgeInsets.all(5),
                width: 90,
                child: Text(item.author.nickName, maxLines: 1, overflow: TextOverflow.ellipsis, style: TextStyle(fontSize: 12),),
              )
            ],
          ),
          Row(
            children: [
              Icon(Icons.thumb_up, size: 14, color: Colors.grey,),
              Padding(
                padding: EdgeInsets.only(left: 3),
                child: Text(item.likeCount.toString(), style: TextStyle(fontSize: 10),),
              )
            ],
          )
        ],
      ),
    );
  }

  @override
  bool get wantKeepAlive => true;

}
