import 'package:flutter/material.dart';
import 'package:flutter_all/api/http_api.dart';
import 'package:flutter_all/api/http_manager.dart';
import 'package:flutter_all/model/search_entity.dart';
import 'package:flutter_all/model/swiper_model.dart';
import 'package:flutter_all/wigets/search_bar.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:shared_preferences/shared_preferences.dart';

const TYPES = [
  'channelplane',
  'district',
  'food',
  'hotel',
  'huodong',
  'shop',
  'sight',
  'ticket',
  'travelgroup',
  'channelgroup',
  'channelgs',
  'channeltrain',
  'cruise'
];

class SearchPage extends StatefulWidget {
  String keyWord;
  SearchPage({this.keyWord});
  @override
  _SearchPageState createState() => _SearchPageState();

}


class _SearchPageState extends State<SearchPage> {

  SearchEntity searchEntity;
  @override
  void initState() {
    if (widget.keyWord != null) {
        _onTextchange(widget.keyWord);
    }
    super.initState();

  }


  @override
  void dispose() {
    FocusScope.of(context).requestFocus(FocusNode());
    super.dispose();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("搜索"),
      ),
      body: Center(
        child: Container(
          child: Column(
            children: [
              SearchBar(
                hideLeft: true,
                defaultText: widget.keyWord,
                hint: '123',
                leftButtonClick: () {
                  Navigator.pop(context);
                },
                onChanged: _onTextchange,
                speakClick: () {
                  print("123123");
                },
              ),
              Expanded(
                flex: 1,
                child: ListView.builder(
                  itemBuilder: (BuildContext context, int position) {
                    return _item(position);
                  },
                  itemCount: searchEntity?.data?.length??0,
                ),
              )
            ],
          ),
        ),
      ),
    );
  }


  _onTextchange(String text) {
    widget.keyWord = text;
    if (text.length == 0) {
      setState(() {
        searchEntity = null;
      });
      return;
    }
    HttpManager.getInstance(baseUrl: HttpApi.BaseUrl_01).getAsync(url: HttpApi.search, tag: "search",
        params: {"source":"mobileweb", "action":"mobileweb", "keyword": widget.keyWord}).then(((result) {
      SearchEntity entity = SearchEntity().fromJson(result);
      setState(() {
        searchEntity = entity;
      });
    }));
  }

  _item(int position) {
    if (searchEntity == null || searchEntity.data == null) return null;
    SearchData searchData = searchEntity.data[position];
    return GestureDetector(
      onTap: () {
        print(searchData.url);
      },
      child: Container(
        padding: EdgeInsets.all(10),
        decoration: BoxDecoration(
          border: Border(bottom: BorderSide(width: 0.3, color: Colors.grey))
        ),
        child: Row(
          children: [
            Container(
              margin: EdgeInsets.all(1),
              child: Image(
                height: 26,
                width: 26,
                image: AssetImage(_typeImg(searchData.type)),
              ),
            ),
            Column(
              children: [
                Container(
                  width: 300,
                  child: _title(searchData),
                ),
                Container(
                  margin: EdgeInsets.only(top: 5),
                  width: 300,
                  child: _subTitle(searchData),
                )
              ],
            )
          ],
        ),
      ),
    );

  }

  _typeImg(String type) {
    print(type);
    // return 'images/type_travelgroup.png';
    if(type == null) return 'images/type_travelgroup.png';
    for (final val in TYPES) {
      if (type == val) {
        return 'images/type_${type}.png';
        break;
      }
    }
    return 'images/type_travelgroup.png';
  }

  _title(SearchData searchData) {
    List<TextSpan>spans = [];
    spans.addAll(_keywordTextSpans(searchData.word, widget.keyWord));
    spans.add(TextSpan(text: ' ' + (searchData.districtname??'') + ' ' + (searchData.zonename??''),
        style: TextStyle(fontSize: 16, color: Colors.grey)),);
    return RichText(text: TextSpan(children: spans),);
    // Text("${searchData.word??''} ${searchData.districtname??''} ${searchData.zonename??''}")
  }

  _subTitle(SearchData searchData) {
    return RichText(
      text: TextSpan(
        children: [
          TextSpan(
            text: searchData.price??'',
            style: TextStyle(fontSize: 16, color: Colors.orange)
          ),
          TextSpan(
            text: ' ' + (searchData.star??''),
            style: TextStyle(fontSize: 12, color: Colors.grey)
          )
        ]
      ),
    );
    Text("${searchData.price??''} ${searchData.type??''}");
  }

  _keywordTextSpans(String word, String keyword) {
    List<TextSpan>spans = [];
    if(word == null) return spans;
    List<String> arr = word.split(keyword);
    TextStyle normalStyle = TextStyle(fontSize: 16, color: Colors.black87);
    TextStyle keyWordsStyle = TextStyle(fontSize: 16, color: Colors.orange);
    for (int i=0; i<arr.length;i++) {
      if((i+1)%2==0) {
        spans.add(TextSpan(text: keyword, style: keyWordsStyle));
      }
      String val = arr[i];
      if(val!=null) {
        spans.add(TextSpan(text: val, style: normalStyle));
      }
    }
    return spans;
  }




  // void _testSharePreferences(bool isSave) async {
  //   final prefs = await SharedPreferences.getInstance();
  //   if (isSave) {
  //     prefs.setString("test", "哈哈");
  //   } else {
  //     final value = prefs.getString("test");
  //     setState(() {
  //       title = value;
  //     });
  //   }
  // }



}


