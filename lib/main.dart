import 'package:flutter/material.dart';
import 'package:flutter_all/api/http_api.dart';
import 'package:flutter_all/api/http_config.dart';
import 'package:flutter_all/api/http_manager.dart';
import 'package:flutter_all/navigator/tab_navigator.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  ScreenUtil.init(designSize: Size(750, 1334), allowFontScaling: false);
  // HttpManager().init(baseUrl: HttpApi.BaesUrl, connectTimeout: HttpConfig.CONNECT_TIMEOUT);
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        // 点击时的高亮效果设置为透明
        splashColor: Colors.transparent,
        // 长按时的扩散效果设置为透明
        highlightColor: Colors.transparent,
      ),
      home: TabNavigator(),
      // routes: ,
      // initialRoute: ,
    );
  }
}

