import 'package:flutter/material.dart';
import 'package:flutter_all/config/ui_config.dart';
import 'package:flutter_all/pages/home_page.dart';
import 'package:flutter_all/pages/my_page.dart';
import 'package:flutter_all/pages/search_page.dart';
import 'package:flutter_all/pages/travel_page.dart';

class TabNavigator extends StatefulWidget {

  TabNavigator({Key key}) : super(key: key); // 构造函数

  @override
  _TabNavigatorState createState() => _TabNavigatorState();
}

class _TabNavigatorState extends State<TabNavigator> {
  var _controller = PageController(initialPage: 0);
  int _currentIndex = 0;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: PageView(
        controller: _controller,
        children: <Widget>[
          HomePage(),
          SearchPage(),
          TravelPage(),
          MyPage()
        ],
        physics: NeverScrollableScrollPhysics(),
      ),
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: _currentIndex,
        onTap: (index) {
          _controller.jumpToPage(index);
          setState(() {
            _currentIndex = index;
          });
        },
        selectedFontSize: 14,
        unselectedFontSize: 14,
        selectedItemColor: UIConfig.tabbarSelectedColor,
        unselectedItemColor: UIConfig.tabbarNormalColor,
        type: BottomNavigationBarType.fixed,
        items: [
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            activeIcon: Icon(Icons.home),
            title: Text("首页"),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.search, color: UIConfig.tabbarNormalColor,),
            activeIcon: Icon(Icons.search, color: UIConfig.tabbarSelectedColor,),
            title: Text("搜索"),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.camera_alt, color: UIConfig.tabbarNormalColor,),
            activeIcon: Icon(Icons.camera_alt, color: UIConfig.tabbarSelectedColor,),
            title: Text("旅拍"),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.person, color: UIConfig.tabbarNormalColor,),
            activeIcon: Icon(Icons.person, color: UIConfig.tabbarSelectedColor,),
            title: Text("我的"),
          )
        ],

      ),
    );
  }
}
