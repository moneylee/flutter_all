import 'package:flutter/material.dart';
import 'package:flutter_all/pages/unknow_page.dart';

class GRRouter {
  static final String initialRoute =  "/login";


  static final Map<String, WidgetBuilder> routes = {
    // GRMainScreen.routeName: (ctx) => GRMainScreen(),
    // GRMealScreen.routeName: (ctx) => GRMealScreen(),
    // GRDetailScreen.routeName: (ctx) => GRDetailScreen(),
  };

  //个别自定义的页面 需要页面在初始化时传参时可以使用此方法 在这里注册页面
  // static final RouteFactory generateRoute = (settings) {
  //   if (settings.name == GRFilterScreen.routerName) {
  //     return MaterialPageRoute(
  //         builder: (ctx) {
  //           return GRFilterScreen();
  //         },
  //         fullscreenDialog: true
  //     );
  //   }
  //   return null;
  // };
  //错误页面
  static final RouteFactory unknownRoute = (settings) {
    return MaterialPageRoute(
        builder: (ctx) {
          return UnknowPage();
        }
    );
  };



}