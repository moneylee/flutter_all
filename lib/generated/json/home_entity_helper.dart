import 'package:flutter_all/model/home_entity.dart';

homeEntityFromJson(HomeEntity data, Map<String, dynamic> json) {
	if (json['config'] != null) {
		data.config = new HomeConfig().fromJson(json['config']);
	}
	if (json['bannerList'] != null) {
		data.bannerList = new List<HomeBannerList>();
		(json['bannerList'] as List).forEach((v) {
			data.bannerList.add(new HomeBannerList().fromJson(v));
		});
	}
	if (json['localNavList'] != null) {
		data.localNavList = new List<HomeLocalNavList>();
		(json['localNavList'] as List).forEach((v) {
			data.localNavList.add(new HomeLocalNavList().fromJson(v));
		});
	}
	if (json['gridNav'] != null) {
		data.gridNav = new HomeGridNav().fromJson(json['gridNav']);
	}
	if (json['subNavList'] != null) {
		data.subNavList = new List<HomeSubNavList>();
		(json['subNavList'] as List).forEach((v) {
			data.subNavList.add(new HomeSubNavList().fromJson(v));
		});
	}
	if (json['salesBox'] != null) {
		data.salesBox = new HomeSalesBox().fromJson(json['salesBox']);
	}
	return data;
}

Map<String, dynamic> homeEntityToJson(HomeEntity entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	if (entity.config != null) {
		data['config'] = entity.config.toJson();
	}
	if (entity.bannerList != null) {
		data['bannerList'] =  entity.bannerList.map((v) => v.toJson()).toList();
	}
	if (entity.localNavList != null) {
		data['localNavList'] =  entity.localNavList.map((v) => v.toJson()).toList();
	}
	if (entity.gridNav != null) {
		data['gridNav'] = entity.gridNav.toJson();
	}
	if (entity.subNavList != null) {
		data['subNavList'] =  entity.subNavList.map((v) => v.toJson()).toList();
	}
	if (entity.salesBox != null) {
		data['salesBox'] = entity.salesBox.toJson();
	}
	return data;
}

homeConfigFromJson(HomeConfig data, Map<String, dynamic> json) {
	if (json['searchUrl'] != null) {
		data.searchUrl = json['searchUrl']?.toString();
	}
	return data;
}

Map<String, dynamic> homeConfigToJson(HomeConfig entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['searchUrl'] = entity.searchUrl;
	return data;
}

homeBannerListFromJson(HomeBannerList data, Map<String, dynamic> json) {
	if (json['icon'] != null) {
		data.icon = json['icon']?.toString();
	}
	if (json['url'] != null) {
		data.url = json['url']?.toString();
	}
	return data;
}

Map<String, dynamic> homeBannerListToJson(HomeBannerList entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['icon'] = entity.icon;
	data['url'] = entity.url;
	return data;
}

homeLocalNavListFromJson(HomeLocalNavList data, Map<String, dynamic> json) {
	if (json['icon'] != null) {
		data.icon = json['icon']?.toString();
	}
	if (json['title'] != null) {
		data.title = json['title']?.toString();
	}
	if (json['url'] != null) {
		data.url = json['url']?.toString();
	}
	if (json['statusBarColor'] != null) {
		data.statusBarColor = json['statusBarColor']?.toString();
	}
	if (json['hideAppBar'] != null) {
		data.hideAppBar = json['hideAppBar'];
	}
	return data;
}

Map<String, dynamic> homeLocalNavListToJson(HomeLocalNavList entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['icon'] = entity.icon;
	data['title'] = entity.title;
	data['url'] = entity.url;
	data['statusBarColor'] = entity.statusBarColor;
	data['hideAppBar'] = entity.hideAppBar;
	return data;
}

homeGridNavFromJson(HomeGridNav data, Map<String, dynamic> json) {
	if (json['hotel'] != null) {
		data.hotel = new HomeGridNavHotel().fromJson(json['hotel']);
	}
	if (json['flight'] != null) {
		data.flight = new HomeGridNavFlight().fromJson(json['flight']);
	}
	if (json['travel'] != null) {
		data.travel = new HomeGridNavTravel().fromJson(json['travel']);
	}
	return data;
}

Map<String, dynamic> homeGridNavToJson(HomeGridNav entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	if (entity.hotel != null) {
		data['hotel'] = entity.hotel.toJson();
	}
	if (entity.flight != null) {
		data['flight'] = entity.flight.toJson();
	}
	if (entity.travel != null) {
		data['travel'] = entity.travel.toJson();
	}
	return data;
}

homeGridNavHotelFromJson(HomeGridNavHotel data, Map<String, dynamic> json) {
	if (json['startColor'] != null) {
		data.startColor = json['startColor']?.toString();
	}
	if (json['endColor'] != null) {
		data.endColor = json['endColor']?.toString();
	}
	if (json['mainItem'] != null) {
		data.mainItem = new HomeGridNavHotelMainItem().fromJson(json['mainItem']);
	}
	if (json['item1'] != null) {
		data.item1 = new HomeGridNavHotelItem1().fromJson(json['item1']);
	}
	if (json['item2'] != null) {
		data.item2 = new HomeGridNavHotelItem2().fromJson(json['item2']);
	}
	if (json['item3'] != null) {
		data.item3 = new HomeGridNavHotelItem3().fromJson(json['item3']);
	}
	if (json['item4'] != null) {
		data.item4 = new HomeGridNavHotelItem4().fromJson(json['item4']);
	}
	return data;
}

Map<String, dynamic> homeGridNavHotelToJson(HomeGridNavHotel entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['startColor'] = entity.startColor;
	data['endColor'] = entity.endColor;
	if (entity.mainItem != null) {
		data['mainItem'] = entity.mainItem.toJson();
	}
	if (entity.item1 != null) {
		data['item1'] = entity.item1.toJson();
	}
	if (entity.item2 != null) {
		data['item2'] = entity.item2.toJson();
	}
	if (entity.item3 != null) {
		data['item3'] = entity.item3.toJson();
	}
	if (entity.item4 != null) {
		data['item4'] = entity.item4.toJson();
	}
	return data;
}

homeGridNavHotelMainItemFromJson(HomeGridNavHotelMainItem data, Map<String, dynamic> json) {
	if (json['title'] != null) {
		data.title = json['title']?.toString();
	}
	if (json['icon'] != null) {
		data.icon = json['icon']?.toString();
	}
	if (json['url'] != null) {
		data.url = json['url']?.toString();
	}
	if (json['statusBarColor'] != null) {
		data.statusBarColor = json['statusBarColor']?.toString();
	}
	return data;
}

Map<String, dynamic> homeGridNavHotelMainItemToJson(HomeGridNavHotelMainItem entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['title'] = entity.title;
	data['icon'] = entity.icon;
	data['url'] = entity.url;
	data['statusBarColor'] = entity.statusBarColor;
	return data;
}

homeGridNavHotelItem1FromJson(HomeGridNavHotelItem1 data, Map<String, dynamic> json) {
	if (json['title'] != null) {
		data.title = json['title']?.toString();
	}
	if (json['url'] != null) {
		data.url = json['url']?.toString();
	}
	if (json['statusBarColor'] != null) {
		data.statusBarColor = json['statusBarColor']?.toString();
	}
	return data;
}

Map<String, dynamic> homeGridNavHotelItem1ToJson(HomeGridNavHotelItem1 entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['title'] = entity.title;
	data['url'] = entity.url;
	data['statusBarColor'] = entity.statusBarColor;
	return data;
}

homeGridNavHotelItem2FromJson(HomeGridNavHotelItem2 data, Map<String, dynamic> json) {
	if (json['title'] != null) {
		data.title = json['title']?.toString();
	}
	if (json['url'] != null) {
		data.url = json['url']?.toString();
	}
	return data;
}

Map<String, dynamic> homeGridNavHotelItem2ToJson(HomeGridNavHotelItem2 entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['title'] = entity.title;
	data['url'] = entity.url;
	return data;
}

homeGridNavHotelItem3FromJson(HomeGridNavHotelItem3 data, Map<String, dynamic> json) {
	if (json['title'] != null) {
		data.title = json['title']?.toString();
	}
	if (json['url'] != null) {
		data.url = json['url']?.toString();
	}
	if (json['hideAppBar'] != null) {
		data.hideAppBar = json['hideAppBar'];
	}
	return data;
}

Map<String, dynamic> homeGridNavHotelItem3ToJson(HomeGridNavHotelItem3 entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['title'] = entity.title;
	data['url'] = entity.url;
	data['hideAppBar'] = entity.hideAppBar;
	return data;
}

homeGridNavHotelItem4FromJson(HomeGridNavHotelItem4 data, Map<String, dynamic> json) {
	if (json['title'] != null) {
		data.title = json['title']?.toString();
	}
	if (json['url'] != null) {
		data.url = json['url']?.toString();
	}
	if (json['hideAppBar'] != null) {
		data.hideAppBar = json['hideAppBar'];
	}
	return data;
}

Map<String, dynamic> homeGridNavHotelItem4ToJson(HomeGridNavHotelItem4 entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['title'] = entity.title;
	data['url'] = entity.url;
	data['hideAppBar'] = entity.hideAppBar;
	return data;
}

homeGridNavFlightFromJson(HomeGridNavFlight data, Map<String, dynamic> json) {
	if (json['startColor'] != null) {
		data.startColor = json['startColor']?.toString();
	}
	if (json['endColor'] != null) {
		data.endColor = json['endColor']?.toString();
	}
	if (json['mainItem'] != null) {
		data.mainItem = new HomeGridNavFlightMainItem().fromJson(json['mainItem']);
	}
	if (json['item1'] != null) {
		data.item1 = new HomeGridNavFlightItem1().fromJson(json['item1']);
	}
	if (json['item2'] != null) {
		data.item2 = new HomeGridNavFlightItem2().fromJson(json['item2']);
	}
	if (json['item3'] != null) {
		data.item3 = new HomeGridNavFlightItem3().fromJson(json['item3']);
	}
	if (json['item4'] != null) {
		data.item4 = new HomeGridNavFlightItem4().fromJson(json['item4']);
	}
	return data;
}

Map<String, dynamic> homeGridNavFlightToJson(HomeGridNavFlight entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['startColor'] = entity.startColor;
	data['endColor'] = entity.endColor;
	if (entity.mainItem != null) {
		data['mainItem'] = entity.mainItem.toJson();
	}
	if (entity.item1 != null) {
		data['item1'] = entity.item1.toJson();
	}
	if (entity.item2 != null) {
		data['item2'] = entity.item2.toJson();
	}
	if (entity.item3 != null) {
		data['item3'] = entity.item3.toJson();
	}
	if (entity.item4 != null) {
		data['item4'] = entity.item4.toJson();
	}
	return data;
}

homeGridNavFlightMainItemFromJson(HomeGridNavFlightMainItem data, Map<String, dynamic> json) {
	if (json['title'] != null) {
		data.title = json['title']?.toString();
	}
	if (json['icon'] != null) {
		data.icon = json['icon']?.toString();
	}
	if (json['url'] != null) {
		data.url = json['url']?.toString();
	}
	return data;
}

Map<String, dynamic> homeGridNavFlightMainItemToJson(HomeGridNavFlightMainItem entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['title'] = entity.title;
	data['icon'] = entity.icon;
	data['url'] = entity.url;
	return data;
}

homeGridNavFlightItem1FromJson(HomeGridNavFlightItem1 data, Map<String, dynamic> json) {
	if (json['title'] != null) {
		data.title = json['title']?.toString();
	}
	if (json['url'] != null) {
		data.url = json['url']?.toString();
	}
	if (json['hideAppBar'] != null) {
		data.hideAppBar = json['hideAppBar'];
	}
	return data;
}

Map<String, dynamic> homeGridNavFlightItem1ToJson(HomeGridNavFlightItem1 entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['title'] = entity.title;
	data['url'] = entity.url;
	data['hideAppBar'] = entity.hideAppBar;
	return data;
}

homeGridNavFlightItem2FromJson(HomeGridNavFlightItem2 data, Map<String, dynamic> json) {
	if (json['title'] != null) {
		data.title = json['title']?.toString();
	}
	if (json['url'] != null) {
		data.url = json['url']?.toString();
	}
	return data;
}

Map<String, dynamic> homeGridNavFlightItem2ToJson(HomeGridNavFlightItem2 entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['title'] = entity.title;
	data['url'] = entity.url;
	return data;
}

homeGridNavFlightItem3FromJson(HomeGridNavFlightItem3 data, Map<String, dynamic> json) {
	if (json['title'] != null) {
		data.title = json['title']?.toString();
	}
	if (json['url'] != null) {
		data.url = json['url']?.toString();
	}
	if (json['hideAppBar'] != null) {
		data.hideAppBar = json['hideAppBar'];
	}
	return data;
}

Map<String, dynamic> homeGridNavFlightItem3ToJson(HomeGridNavFlightItem3 entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['title'] = entity.title;
	data['url'] = entity.url;
	data['hideAppBar'] = entity.hideAppBar;
	return data;
}

homeGridNavFlightItem4FromJson(HomeGridNavFlightItem4 data, Map<String, dynamic> json) {
	if (json['title'] != null) {
		data.title = json['title']?.toString();
	}
	if (json['url'] != null) {
		data.url = json['url']?.toString();
	}
	if (json['hideAppBar'] != null) {
		data.hideAppBar = json['hideAppBar'];
	}
	return data;
}

Map<String, dynamic> homeGridNavFlightItem4ToJson(HomeGridNavFlightItem4 entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['title'] = entity.title;
	data['url'] = entity.url;
	data['hideAppBar'] = entity.hideAppBar;
	return data;
}

homeGridNavTravelFromJson(HomeGridNavTravel data, Map<String, dynamic> json) {
	if (json['startColor'] != null) {
		data.startColor = json['startColor']?.toString();
	}
	if (json['endColor'] != null) {
		data.endColor = json['endColor']?.toString();
	}
	if (json['mainItem'] != null) {
		data.mainItem = new HomeGridNavTravelMainItem().fromJson(json['mainItem']);
	}
	if (json['item1'] != null) {
		data.item1 = new HomeGridNavTravelItem1().fromJson(json['item1']);
	}
	if (json['item2'] != null) {
		data.item2 = new HomeGridNavTravelItem2().fromJson(json['item2']);
	}
	if (json['item3'] != null) {
		data.item3 = new HomeGridNavTravelItem3().fromJson(json['item3']);
	}
	if (json['item4'] != null) {
		data.item4 = new HomeGridNavTravelItem4().fromJson(json['item4']);
	}
	return data;
}

Map<String, dynamic> homeGridNavTravelToJson(HomeGridNavTravel entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['startColor'] = entity.startColor;
	data['endColor'] = entity.endColor;
	if (entity.mainItem != null) {
		data['mainItem'] = entity.mainItem.toJson();
	}
	if (entity.item1 != null) {
		data['item1'] = entity.item1.toJson();
	}
	if (entity.item2 != null) {
		data['item2'] = entity.item2.toJson();
	}
	if (entity.item3 != null) {
		data['item3'] = entity.item3.toJson();
	}
	if (entity.item4 != null) {
		data['item4'] = entity.item4.toJson();
	}
	return data;
}

homeGridNavTravelMainItemFromJson(HomeGridNavTravelMainItem data, Map<String, dynamic> json) {
	if (json['title'] != null) {
		data.title = json['title']?.toString();
	}
	if (json['icon'] != null) {
		data.icon = json['icon']?.toString();
	}
	if (json['url'] != null) {
		data.url = json['url']?.toString();
	}
	if (json['hideAppBar'] != null) {
		data.hideAppBar = json['hideAppBar'];
	}
	if (json['statusBarColor'] != null) {
		data.statusBarColor = json['statusBarColor']?.toString();
	}
	return data;
}

Map<String, dynamic> homeGridNavTravelMainItemToJson(HomeGridNavTravelMainItem entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['title'] = entity.title;
	data['icon'] = entity.icon;
	data['url'] = entity.url;
	data['hideAppBar'] = entity.hideAppBar;
	data['statusBarColor'] = entity.statusBarColor;
	return data;
}

homeGridNavTravelItem1FromJson(HomeGridNavTravelItem1 data, Map<String, dynamic> json) {
	if (json['title'] != null) {
		data.title = json['title']?.toString();
	}
	if (json['url'] != null) {
		data.url = json['url']?.toString();
	}
	if (json['statusBarColor'] != null) {
		data.statusBarColor = json['statusBarColor']?.toString();
	}
	if (json['hideAppBar'] != null) {
		data.hideAppBar = json['hideAppBar'];
	}
	return data;
}

Map<String, dynamic> homeGridNavTravelItem1ToJson(HomeGridNavTravelItem1 entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['title'] = entity.title;
	data['url'] = entity.url;
	data['statusBarColor'] = entity.statusBarColor;
	data['hideAppBar'] = entity.hideAppBar;
	return data;
}

homeGridNavTravelItem2FromJson(HomeGridNavTravelItem2 data, Map<String, dynamic> json) {
	if (json['title'] != null) {
		data.title = json['title']?.toString();
	}
	if (json['url'] != null) {
		data.url = json['url']?.toString();
	}
	if (json['statusBarColor'] != null) {
		data.statusBarColor = json['statusBarColor']?.toString();
	}
	if (json['hideAppBar'] != null) {
		data.hideAppBar = json['hideAppBar'];
	}
	return data;
}

Map<String, dynamic> homeGridNavTravelItem2ToJson(HomeGridNavTravelItem2 entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['title'] = entity.title;
	data['url'] = entity.url;
	data['statusBarColor'] = entity.statusBarColor;
	data['hideAppBar'] = entity.hideAppBar;
	return data;
}

homeGridNavTravelItem3FromJson(HomeGridNavTravelItem3 data, Map<String, dynamic> json) {
	if (json['title'] != null) {
		data.title = json['title']?.toString();
	}
	if (json['url'] != null) {
		data.url = json['url']?.toString();
	}
	if (json['hideAppBar'] != null) {
		data.hideAppBar = json['hideAppBar'];
	}
	return data;
}

Map<String, dynamic> homeGridNavTravelItem3ToJson(HomeGridNavTravelItem3 entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['title'] = entity.title;
	data['url'] = entity.url;
	data['hideAppBar'] = entity.hideAppBar;
	return data;
}

homeGridNavTravelItem4FromJson(HomeGridNavTravelItem4 data, Map<String, dynamic> json) {
	if (json['title'] != null) {
		data.title = json['title']?.toString();
	}
	if (json['url'] != null) {
		data.url = json['url']?.toString();
	}
	if (json['hideAppBar'] != null) {
		data.hideAppBar = json['hideAppBar'];
	}
	return data;
}

Map<String, dynamic> homeGridNavTravelItem4ToJson(HomeGridNavTravelItem4 entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['title'] = entity.title;
	data['url'] = entity.url;
	data['hideAppBar'] = entity.hideAppBar;
	return data;
}

homeSubNavListFromJson(HomeSubNavList data, Map<String, dynamic> json) {
	if (json['icon'] != null) {
		data.icon = json['icon']?.toString();
	}
	if (json['title'] != null) {
		data.title = json['title']?.toString();
	}
	if (json['url'] != null) {
		data.url = json['url']?.toString();
	}
	if (json['hideAppBar'] != null) {
		data.hideAppBar = json['hideAppBar'];
	}
	return data;
}

Map<String, dynamic> homeSubNavListToJson(HomeSubNavList entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['icon'] = entity.icon;
	data['title'] = entity.title;
	data['url'] = entity.url;
	data['hideAppBar'] = entity.hideAppBar;
	return data;
}

homeSalesBoxFromJson(HomeSalesBox data, Map<String, dynamic> json) {
	if (json['icon'] != null) {
		data.icon = json['icon']?.toString();
	}
	if (json['moreUrl'] != null) {
		data.moreUrl = json['moreUrl']?.toString();
	}
	if (json['bigCard1'] != null) {
		data.bigCard1 = new HomeSalesBoxBigCard1().fromJson(json['bigCard1']);
	}
	if (json['bigCard2'] != null) {
		data.bigCard2 = new HomeSalesBoxBigCard2().fromJson(json['bigCard2']);
	}
	if (json['smallCard1'] != null) {
		data.smallCard1 = new HomeSalesBoxSmallCard1().fromJson(json['smallCard1']);
	}
	if (json['smallCard2'] != null) {
		data.smallCard2 = new HomeSalesBoxSmallCard2().fromJson(json['smallCard2']);
	}
	if (json['smallCard3'] != null) {
		data.smallCard3 = new HomeSalesBoxSmallCard3().fromJson(json['smallCard3']);
	}
	if (json['smallCard4'] != null) {
		data.smallCard4 = new HomeSalesBoxSmallCard4().fromJson(json['smallCard4']);
	}
	return data;
}

Map<String, dynamic> homeSalesBoxToJson(HomeSalesBox entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['icon'] = entity.icon;
	data['moreUrl'] = entity.moreUrl;
	if (entity.bigCard1 != null) {
		data['bigCard1'] = entity.bigCard1.toJson();
	}
	if (entity.bigCard2 != null) {
		data['bigCard2'] = entity.bigCard2.toJson();
	}
	if (entity.smallCard1 != null) {
		data['smallCard1'] = entity.smallCard1.toJson();
	}
	if (entity.smallCard2 != null) {
		data['smallCard2'] = entity.smallCard2.toJson();
	}
	if (entity.smallCard3 != null) {
		data['smallCard3'] = entity.smallCard3.toJson();
	}
	if (entity.smallCard4 != null) {
		data['smallCard4'] = entity.smallCard4.toJson();
	}
	return data;
}

homeSalesBoxBigCard1FromJson(HomeSalesBoxBigCard1 data, Map<String, dynamic> json) {
	if (json['icon'] != null) {
		data.icon = json['icon']?.toString();
	}
	if (json['url'] != null) {
		data.url = json['url']?.toString();
	}
	if (json['title'] != null) {
		data.title = json['title']?.toString();
	}
	return data;
}

Map<String, dynamic> homeSalesBoxBigCard1ToJson(HomeSalesBoxBigCard1 entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['icon'] = entity.icon;
	data['url'] = entity.url;
	data['title'] = entity.title;
	return data;
}

homeSalesBoxBigCard2FromJson(HomeSalesBoxBigCard2 data, Map<String, dynamic> json) {
	if (json['icon'] != null) {
		data.icon = json['icon']?.toString();
	}
	if (json['url'] != null) {
		data.url = json['url']?.toString();
	}
	if (json['title'] != null) {
		data.title = json['title']?.toString();
	}
	return data;
}

Map<String, dynamic> homeSalesBoxBigCard2ToJson(HomeSalesBoxBigCard2 entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['icon'] = entity.icon;
	data['url'] = entity.url;
	data['title'] = entity.title;
	return data;
}

homeSalesBoxSmallCard1FromJson(HomeSalesBoxSmallCard1 data, Map<String, dynamic> json) {
	if (json['icon'] != null) {
		data.icon = json['icon']?.toString();
	}
	if (json['url'] != null) {
		data.url = json['url']?.toString();
	}
	if (json['title'] != null) {
		data.title = json['title']?.toString();
	}
	return data;
}

Map<String, dynamic> homeSalesBoxSmallCard1ToJson(HomeSalesBoxSmallCard1 entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['icon'] = entity.icon;
	data['url'] = entity.url;
	data['title'] = entity.title;
	return data;
}

homeSalesBoxSmallCard2FromJson(HomeSalesBoxSmallCard2 data, Map<String, dynamic> json) {
	if (json['icon'] != null) {
		data.icon = json['icon']?.toString();
	}
	if (json['url'] != null) {
		data.url = json['url']?.toString();
	}
	if (json['title'] != null) {
		data.title = json['title']?.toString();
	}
	return data;
}

Map<String, dynamic> homeSalesBoxSmallCard2ToJson(HomeSalesBoxSmallCard2 entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['icon'] = entity.icon;
	data['url'] = entity.url;
	data['title'] = entity.title;
	return data;
}

homeSalesBoxSmallCard3FromJson(HomeSalesBoxSmallCard3 data, Map<String, dynamic> json) {
	if (json['icon'] != null) {
		data.icon = json['icon']?.toString();
	}
	if (json['url'] != null) {
		data.url = json['url']?.toString();
	}
	if (json['title'] != null) {
		data.title = json['title']?.toString();
	}
	return data;
}

Map<String, dynamic> homeSalesBoxSmallCard3ToJson(HomeSalesBoxSmallCard3 entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['icon'] = entity.icon;
	data['url'] = entity.url;
	data['title'] = entity.title;
	return data;
}

homeSalesBoxSmallCard4FromJson(HomeSalesBoxSmallCard4 data, Map<String, dynamic> json) {
	if (json['icon'] != null) {
		data.icon = json['icon']?.toString();
	}
	if (json['url'] != null) {
		data.url = json['url']?.toString();
	}
	if (json['title'] != null) {
		data.title = json['title']?.toString();
	}
	return data;
}

Map<String, dynamic> homeSalesBoxSmallCard4ToJson(HomeSalesBoxSmallCard4 entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['icon'] = entity.icon;
	data['url'] = entity.url;
	data['title'] = entity.title;
	return data;
}