import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';

//拦截白名单
const CATCH_URLS = ["m.ctrip.com/", 'm.ctrip.com/html5/', 'm.ctrip.com/html5/'];

class WebView extends StatefulWidget {
  final String url;
  final String title;
  final bool hedeAppBar;
  final bool backForbid;
  @override
  _WebViewState createState() => _WebViewState();
  WebView({this.url, this.title, this.hedeAppBar, this.backForbid});
}

class _WebViewState extends State<WebView> {
  final webviewReference = FlutterWebviewPlugin();
  StreamSubscription<String> _onUrlChanged;
  StreamSubscription<WebViewStateChanged> _onStateChanged;
  StreamSubscription<WebViewHttpError> _httpError;
  bool exiting = false;

  @override
  void initState() {
    super.initState();
    webviewReference.close();
    _onUrlChanged = webviewReference.onUrlChanged.listen((String url) {
      // print(Uri.decodeComponent(url));
    });
    _onStateChanged = webviewReference.onStateChanged.listen((WebViewStateChanged webViewStateChanged) {
      switch(webViewStateChanged.type) {
        case WebViewState.startLoad:
          if (_isToMain(webViewStateChanged.url) && !exiting) {
            if (widget.backForbid) {
              webviewReference.launch(widget.url);
            } else {
              Navigator.pop(context);
              exiting = true;
            }
          }
          break;
        default:
          break;
      }
    });
    _httpError = webviewReference.onHttpError.listen((WebViewHttpError webViewHttpError) {
      print(webViewHttpError);
    });
  }

  bool _isToMain(String url) {
    url = Uri.decodeComponent(url);
    if (url == null) return false;
    bool contain = false;
    for(var value in CATCH_URLS) {
      if (url.endsWith(value) ?? false) {
        contain = true;
        break;
      }
    }
    return contain;
  }

  @override
  void dispose() {
    _onUrlChanged.cancel();
    _onStateChanged.cancel();
    _httpError.cancel();
    webviewReference.dispose();
    super.dispose();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          _appBar(),
          Expanded(
            child: WebviewScaffold(
              url: widget.url,
              withZoom: true,
              withLocalStorage: true,
              hidden: true,
              initialChild: Container(
                child: Center(
                  child: Text("加载中..."),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _appBar() {
    if (widget.hedeAppBar == true) {
      return Container(
        color: Colors.white,
        height: 30,
      );
    }
    return Container(
      height: 60,
      child: FractionallySizedBox(
        widthFactor: 1,
        child: Stack(
          children: [
            GestureDetector(
              onTap: () {
                Navigator.pop(context);
              },
              child: Container(
                margin: EdgeInsets.only(left: 10, top: 20),
                child: Icon(Icons.close, size: 26,),
              ),
            ),
            Positioned(
              top: 20,
              left: 1,
              right: 1,
              child: Center(
                child: Text(widget.title??"", style: TextStyle(fontSize: 20),),
              ),
            )
          ],
        ),
      ),
    );

  }
}
