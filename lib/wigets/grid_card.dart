import 'package:flutter/material.dart';
import 'package:flutter_all/model/grid_nav_model.dart';
import 'package:flutter_all/model/home_entity.dart';

import 'web_view.dart';

class GridCard extends StatelessWidget {

  final HomeGridNav gridNavModel;
  GridCard(this.gridNavModel);

  @override
  Widget build(BuildContext context) {
    return PhysicalModel(
      child: Column(
        children: _gridCardItems(context),
      ),
      color: Colors.transparent,
      borderRadius: BorderRadius.circular(8),
      clipBehavior: Clip.antiAlias,
    );
  }

  List<Widget> _gridCardItems(BuildContext context) {
    List<Widget> items = [];
    if (gridNavModel == null) return [];
    if (gridNavModel.hotel != null) {
      items.add(_gridCardItem(context, gridNavModel, true, "hotel"));
    }
    if (gridNavModel.flight != null) {
      items.add(_gridCardItem(context, gridNavModel, false, "flight"));
    }
    if (gridNavModel.travel != null) {
      items.add(_gridCardItem(context, gridNavModel, false, "travel"));
    }
    return items;
  }

  _gridCardItem(BuildContext context, HomeGridNav homeGridNav, bool first, String type) {
    var icon = "";
    var title = "";
    var url = "";
    var item1Title = "";
    var item1Url = "";
    var item2Title = "";
    var item2Url = "";
    var item3Title = "";
    var item3Url = "";
    var item4Title = "";
    var item4Url = "";
    Color startColor;
    Color endColor;


    if (type == "hotel") {
      icon = homeGridNav.hotel.mainItem.icon;
      title = homeGridNav.hotel.mainItem.title;
      url = homeGridNav.hotel.mainItem.url;
      item1Title = homeGridNav.hotel.item1.title;
      item1Url = homeGridNav.hotel.item1.url;
      item2Title = homeGridNav.hotel.item2.title;
      item2Url = homeGridNav.hotel.item2.url;
      item3Title = homeGridNav.hotel.item3.title;
      item3Url = homeGridNav.hotel.item3.url;
      item4Title = homeGridNav.hotel.item4.title;
      item4Url = homeGridNav.hotel.item4.url;
      startColor = Color(int.parse('0xff'+ gridNavModel.hotel.startColor));
      endColor = Color(int.parse('0xff'+ gridNavModel.hotel.endColor));

    }
    if (type == "flight") {
      icon = homeGridNav.flight.mainItem.icon;
      title = homeGridNav.flight.mainItem.title;
      url = homeGridNav.flight.mainItem.url;
      item1Title = homeGridNav.flight.item1.title;
      item1Url = homeGridNav.flight.item1.url;
      item2Title = homeGridNav.flight.item2.title;
      item2Url = homeGridNav.flight.item2.url;
      item3Title = homeGridNav.flight.item3.title;
      item3Url = homeGridNav.flight.item3.url;
      item4Title = homeGridNav.flight.item4.title;
      item4Url = homeGridNav.flight.item4.url;
      startColor = Color(int.parse('0xff'+ gridNavModel.flight.startColor));
      endColor = Color(int.parse('0xff'+ gridNavModel.flight.endColor));
    }
    if (type == "travel") {
      icon = homeGridNav.travel.mainItem.icon;
      title = homeGridNav.travel.mainItem.title;
      url = homeGridNav.travel.mainItem.url;
      item1Title = homeGridNav.travel.item1.title;
      item1Url = homeGridNav.travel.item1.url;
      item2Title = homeGridNav.travel.item2.title;
      item2Url = homeGridNav.travel.item2.url;
      item3Title = homeGridNav.travel.item3.title;
      item3Url = homeGridNav.travel.item3.url;
      item4Title = homeGridNav.travel.item4.title;
      item4Url = homeGridNav.travel.item4.url;
      startColor = Color(int.parse('0xff'+ gridNavModel.travel.startColor));
      endColor = Color(int.parse('0xff'+ gridNavModel.travel.endColor));
    }
    List<Widget> items = [];
    items.add(_mainItem(context, homeGridNav, type, title, icon, url));
    items.add(_doubleItem(context, item1Url, item1Title, item2Url, item2Title, true));
    items.add(_doubleItem(context, item3Url, item3Title, item4Url, item4Title, false));
    List<Widget> expandItems = [];
    items.forEach((element) {
      expandItems.add(Expanded(child: element, flex: 1));
    });
    return Container(
      height: 88,
      margin: first?null:EdgeInsets.only(top: 3),
      decoration: BoxDecoration(
        //线性渐变
        gradient: LinearGradient(
          colors: [startColor, endColor]
        )
      ),
      child: Row(children: expandItems,),
    );
  }

  _mainItem(BuildContext context, HomeGridNav homeGridNav, String type, String title, String icon, String url) {
    return _wrapGesture(context, Stack(
      alignment: AlignmentDirectional.topCenter,
      children: [
        Image.network(icon, fit: BoxFit.contain, width: 121, height: 88, alignment: AlignmentDirectional.bottomEnd,),
        Container(
          margin: EdgeInsets.only(top: 11),
          child: Text(title, style: TextStyle(color: Colors.white),))
      ],
    ), url, title);
  }

  _doubleItem(BuildContext context, String url1, String title1, String url2, String title2, bool isCenter) {
    return Column(
      children: [
        Expanded(
          child: _item(context, url1, title1, true, isCenter),
        ),
        Expanded(
          child: _item(context, url2, title2, false, isCenter),
        )
      ],
    );
  }

  _item(BuildContext context, String url, String title, bool first, bool isCenter) {
    BorderSide borderSide = BorderSide(width: 0.8, color: Colors.white);
    return FractionallySizedBox(
      widthFactor: 1,
      child: Container(
        decoration: BoxDecoration(
          border: Border(
            left: borderSide,
            bottom: first?borderSide:BorderSide.none
          )
        ),
        child: _wrapGesture(context, Center(
          child: Text(title, textAlign: TextAlign.center, style: TextStyle(fontSize: 14, color: Colors.white),),
        ), url, title)
      ),
    );
  }

  _wrapGesture(BuildContext context, Widget widget, String url, String title) {
    return GestureDetector(
      onTap: () {
        Navigator.push(context,
            MaterialPageRoute(builder: (ctx) {
              return WebView(url: url, hedeAppBar: false, title: title,);
            })
        );
      },
      child: widget,
    );
  }


}
