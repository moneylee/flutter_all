import 'package:flutter_all/model/home_entity.dart';
import 'package:flutter_all/wigets/web_view.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter/material.dart';

class SubNavWidget extends StatelessWidget {

  final List<HomeSubNavList> subNavList;
  SubNavWidget({this.subNavList});

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(6),
      ),
      child: Padding(
        padding: EdgeInsets.all(7),
        child: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              //_items(context).sublist(0, 5)
              children: _items(context).length > 5 ? _items(context).sublist(0, 5): [],
            ),
            Padding(
              padding: const EdgeInsets.only(top: 8),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: _items(context).length > 5 ? _items(context).sublist(5, subNavList.length) : [],
              ),
            ),
          ],
        )
      ),

    );
  }

  List<Widget> _items(BuildContext context) {
    if (subNavList.length == 0) return [];
    return subNavList.map((e) {
      return _item(context, e);
    }).toList();
  }

  Widget _item(BuildContext context, HomeSubNavList homeSubNavList) {
    return Expanded(
      flex: 1,
      child: Container(
        // color: Colors.yellow,
        child: InkWell(
          onTap: () {
            Navigator.push(context,
              MaterialPageRoute(builder: (ctx) {
                print(homeSubNavList.toJson());
                return WebView(url: homeSubNavList.url, hedeAppBar: homeSubNavList.hideAppBar, title: homeSubNavList.title,);
              })
            );
          },
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Image.network(homeSubNavList.icon, width: 40.w, height: 40.w,),
              SizedBox(height: 5,),
              Text(homeSubNavList.title, style: TextStyle(fontSize: 12),)
            ],
          ),
        ),
      ),
    );
  }

}
