import 'package:flutter_all/model/home_entity.dart';
import 'package:flutter_all/wigets/web_view.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter/material.dart';

class GridNavWidget extends StatelessWidget {

  final List<HomeLocalNavList> localNavList;
  GridNavWidget({this.localNavList});

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(6),
      ),
      child: Padding(
        padding: EdgeInsets.all(7),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: _items(context),
        ),
      ),

    );
  }

  List<Widget> _items(BuildContext context) {
    if (localNavList.length == 0) return [];
    return localNavList.map((e) {
        return _item(context, e);
    }).toList();
  }

  Widget _item(BuildContext context, HomeLocalNavList homeLocalNavList) {
    return Container(
      child: InkWell(
        onTap: () {
          Navigator.push(context,
            MaterialPageRoute(builder: (ctx) {
              print(homeLocalNavList.toJson());
              return WebView(url: homeLocalNavList.url, hedeAppBar: homeLocalNavList.hideAppBar, title: homeLocalNavList.title,);
            })
          );
        },
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Image.network(homeLocalNavList.icon, width: 64.w, height: 64.w,),
            SizedBox(height: 5,),
            Text(homeLocalNavList.title, style: TextStyle(fontSize: 12),)
          ],
        ),
      ),
    );
  }

}
