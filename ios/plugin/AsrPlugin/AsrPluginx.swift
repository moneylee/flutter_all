//
//  AsrPlugin.swift
//  Runner
//
//  Created by 李财 on 2020/10/20.
//

//import UIKit
//import Flutter
//
//class AsrPlugin: NSObject, FlutterPlugin {
//
//    var asrManager = AsrManager();
//    var result = FlutterResult;
//
//    static func register(with registrar: FlutterPluginRegistrar) {
//        let channel = FlutterMethodChannel(name: "asr_plugin", binaryMessenger: registrar.messenger())
//        let instance = AsrPlugin()
//        registrar.addMethodCallDelegate(instance, channel: channel)
//    }
//
//
//    func handle(_ call: FlutterMethodCall, result: @escaping FlutterResult) {
//        if "start" == call.method {
//            self.result = result;
//            self.asrManager.start()
//        } else if "stop" == call.method {
//            self.asrManager.stop()
//        } else if "cancel" == call.method {
//            self.asrManager.cancel();
//        } else {
//            result(FlutterMethodNotImplemented)
//        }
//    }
//
//    func _asrManager() -> AsrManager {
//
//        if self.asrManager != nil {
//            return self.asrManager;
//        }
//        self.asrManager = AsrManager.initWith({ (message) in
//            if (self.result) {
//                self.result(message)
//                self.result == nil;
//            }
//        }, failure: { (error) in
//            if (self.result) {
//                self.result(FlutterError.init(code: "ASR_Fail", message: error, details: nil))
//                self.result == nil;
//            }
//        });
//    }
//
//
//}
