//
//  AsrManager.h
//  Runner
//
//  Created by 李财 on 2020/10/19.
//

#import <Foundation/Foundation.h>
#import "BDSEventManager.h"
#import "BDSASRDefines.h"
#import "BDSASRParameters.h"
@interface AsrManager : NSObject

@property (nonatomic,strong)void(^voiceBlock)(NSString * voiceText);

typedef void(^AsrCallback)(NSString* message);

+(instancetype)initWith:(AsrCallback)success failure:(AsrCallback)failure;
-(void)start;
-(void)stop;
-(void)cancel;

@end

